import { Component } from '@angular/core';
import { Hero } from './Hero';
 
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})

export class AppComponent {

  heroes: Hero[];
  myHero:Hero;
  
  constructor() {
      this.heroes = [
        { id: 11, name: 'Mr. Nice' },
        { id: 12, name: 'Narcoxxxx' },
        { id: 13, name: 'Bombasto' },
        { id: 14, name: 'Celeritas' },
        { id: 15, name: 'Magneta' },
        { id: 16, name: 'RubberMan' },
        { id: 17, name: 'Dynama' },
        { id: 18, name: 'Dr IQ' },
        { id: 19, name: 'Magma' },
        { id: 20, name: 'Tornado' }
      ];    
      this.myHero = this.heroes[1];
  } 


 
  AddHero() {
    //this.heroes.push(new Hero(21, 'Andrea'));
    this.heroes.push({ id: 21, name: 'Andrea' });
  }

  RemoveHero() {
    this.heroes.pop();
  }

  onChange(newValue){
    this.myHero= newValue;
    console.log(this.heroes);
  }

}